import { createConnection, ConnectionPool, closeConnection, createRequest, Request  } from '../database_handler';

describe('Database creation', () => {

    let connection: ConnectionPool;

    beforeAll(async () => {
        connection = await createConnection();
    });

    afterAll(async () => {
        closeConnection(connection);
    });

    test('Database TestBD exists', async () => {
        
        const request: Request = createRequest(connection);
        
        const { rowsAffected, recordset } = await request.query("SELECT [name] FROM [sys].[databases] WHERE [name] = 'TestDB';"); 
        
        expect(rowsAffected[0]).toBe(1);
        expect(recordset[0].name).toBe('TestDB');
    });

});
