# FROM mcr.microsoft.com/mssql/server:2022-latest
FROM mcr.microsoft.com/mssql/server:2022-preview-ubuntu-22.04

ENV MSSQL_PID="Developer"
ENV ACCEPT_EULA="Y"
ENV MSSQL_SA_PASSWORD="Gx7Z2c6S4UN23Gs5"
ENV MSSQL_AGENT_ENABLED="true"

# Expose the SQL Server port.
EXPOSE 1433


# Create app directory
WORKDIR /usr/src/app

# Copy the SQL script and the entrypoint script into the image
COPY create_database.docker.sql entrypoint.sh ./
USER root
# Grant permissions to execute the scripts
RUN chmod +x ./entrypoint.sh ./create_database.docker.sql
USER 1001
# Set the entrypoint script to run on container startup
ENTRYPOINT ["./entrypoint.sh"]