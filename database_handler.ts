import 'dotenv/config';
import { connect, Request, ConnectionPool, config, Transaction } from 'mssql';
// Re-export types from 'mssql'
export { BigInt, NVarChar, DateTime, Int, Decimal, Bit, Date, ConnectionPool, Request, Transaction } from 'mssql';

const server = process.env['MSSQL_SERVER'];
if (server === undefined)
    throw new Error('Environment variable server is not defined')


const sqlConfig: config = {
    server: server,
    database: process.env['DATABASE'],
    user: process.env['MSSQL_SA_USER'],
    password: process.env['MSSQL_SA_PASSWORD'],
    connectionTimeout:20000, // remove later
    requestTimeout: 20000, // remove later
    options: {
        encrypt: true, // for azure
        trustServerCertificate: true // change to true for local dev / self-signed certs
    }
};

/**
 *
 * Establish a connection with the database. 
 *
 * @returns a new connection.
 */
export async function createConnection(): Promise<ConnectionPool> {
    return await connect(sqlConfig);
}

/**
 * 
 * End the connection with the database. 
 *
 * @param {*} connection - connection with the database.
 */
export function closeConnection(connection: ConnectionPool) {

    if (connection) {
        connection.close();
    }

}

export function createRequest(connection: ConnectionPool): Request {

    if(!connection)
        throw new Error('Connection must be established before creating a request.');
    
    return new Request(connection);
}

export function createTransaction(connection: ConnectionPool): Transaction{
    
    if(!connection)
        throw new Error('Connection must be established before creating a transaction.');
    
    return new Transaction(connection);
}
