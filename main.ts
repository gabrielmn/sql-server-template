import { readFile, writeFile } from 'fs/promises';
import { join } from 'path';


async function main() {

    await createForDocker();
    await createForAzure();

}

async function createForDocker() {
 
    let script:string = '', temp:Buffer;
    // load database
    temp = await readFile(join(__dirname, './database/create_database.sql'));
    script += temp.toString();
        
    writeFile(join(__dirname,'./create_database.docker.sql'), script);

}


async function createForAzure() {

    let script:string = '', temp:Buffer;
        
    writeFile(join(__dirname,'./create_database.azure.sql'), script);
}

main();
