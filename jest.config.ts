import type { Config } from '@jest/types';

// Sync object
const config: Config.InitialOptions = {
    verbose: true,
    displayName: {
        name: 'Database',
        color: 'blue'
    },
    preset: 'ts-jest',
    testEnvironment: 'node',
    // uncomment this in case of timeout that cant be avoided.
    testTimeout: 50000
};

export default config;
